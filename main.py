# Importation des biblio nécessaires pour l'interface graphique, le scan réseau, le ping, et la manipulation XML
import tkinter as tk
import nmap
from tkinter import ttk
from ping3 import ping
import xml.etree.ElementTree as ET
from client_API import send_data_to_server
from client_API import send_status_to_server
import socket
from datetime import datetime


class NetworkScannerApp:
    # Initialisation de l'interface graphique principale

    def __init__(self, root):
        self.root = root
        self.root.title("Network Scanner")

        self.tree = ttk.Treeview(root)
        self.tree["columns"] = ("IP", "Ports Ouverts", "Nom")

        self.tree.column("#0", width=0, stretch=tk.NO)
        self.tree.column("IP", anchor=tk.W, width=120)
        self.tree.column("Ports Ouverts", anchor=tk.W, width=200)
        self.tree.column("Nom", anchor=tk.W, width=120)

        self.tree.heading("#0", text="", anchor=tk.W)
        self.tree.heading("IP", text="IP", anchor=tk.W)
        self.tree.heading("Ports Ouverts", text="Ports Ouverts", anchor=tk.W)
        self.tree.heading("Nom", text="Nom", anchor=tk.W)

        self.tree.grid(row=0, rowspan=2, column=0, columnspan=3, sticky=tk.W + tk.E)

        # Créer un cadre pour les informations de la machine
        self.machine_info_frame = tk.LabelFrame(root, text="Informations de la machine")
        self.machine_info_frame.grid(row=0, column=3, sticky=tk.N)

        self.host_count_label = tk.Label(root, text="Nombre d'hôtes scannés: 0")
        self.host_count_label.grid(row=4, column=0, sticky=tk.W)

        self.version_label = tk.Label(root, text="Version : 1.1")
        self.version_label.grid(row=5, column=0, sticky=tk.W)

        # Afficher l'IP et le nom d'hôte de la machine actuelle
        self.ip_label = tk.Label(self.machine_info_frame, text=f"Adresse IP: {self.get_ip()}")
        self.ip_label.grid(row=1, column=0, sticky=tk.W)

        self.hostname_label = tk.Label(self.machine_info_frame, text=f"Nom d'hôte: {self.get_hostname()}")
        self.hostname_label.grid(row=2, column=0, sticky=tk.W)

        self.average_label = tk.Label(root, text="Moyenne du ping:")
        self.average_label.grid(row=2, column=3, sticky=tk.E)

        self.average_text = tk.Entry(root)
        self.average_text.grid(row=3, column=3, sticky=tk.E)

        self.network_label = tk.Label(root, text="Réseau à scanner:")
        self.network_label.grid(row=4, column=3, sticky=tk.E)

        self.network_entry = tk.Entry(root)
        self.network_entry.grid(row=5, column=3, sticky=tk.E)

        self.scan_button = tk.Button(root, text="Lancer le scan", command=self.scan_network)
        self.scan_button.grid(row=6, column=3, sticky=tk.E)

        self.export_button = tk.Button(root, text="Exporter", command=self.export_to_xml)
        self.export_button.grid(row=6, column=1, sticky=tk.W)

        self.server_button = tk.Button(root, text="Envoyer au serveur", command=self.export_to_serveur)
        self.server_button.grid(row=6, column=2, sticky=tk.W)

        self.schedule_status_update()

        # Initialiser une liste pour stocker les résultats du scan
        self.scan_results = []

        # Fonction pour envoyer périodiquement le statut de la sonde au serveur
        # Création et envoi d'un document XML au serveur

    def schedule_status_update(self):
        # Appeler la fonction send_status_to_api toutes les 60 secondes

        # Créer un élément racine pour le document XML
        root = ET.Element("info_sonde")

        # Ajouter la date et l'heure actuelles
        timestamp_element = ET.SubElement(root, "timestamp")
        timestamp_element.text = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # Ajouter l'adresse IP de la sonde
        ip_machine_element = ET.SubElement(root, "ip_sonde")
        ip_machine_element.text = self.get_ip()

        status_element = ET.SubElement(root, "status")
        status_element.text = "Actif"

        # Convertir l'élément racine en une chaîne XML
        xml_content = ET.tostring(root).decode()

        # Envoyer les données au serveur
        send_status_to_server(xml_content)

        # Répéter cette opération toutes les 30 minutes
        self.root.after(1 * 60 * 1000, self.schedule_status_update)

    # Méthode pour obtenir l'adresse IP de la machine harvester
    def get_ip(self):
        """Obtenir l'adresse IP de la machine."""
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            # Utilisez une adresse externe pour trouver l'IP locale sans établir une connexion
            s.connect(('8.8.8.8', 80))
            IP = s.getsockname()[0]
        except Exception:
            IP = '127.0.0.1'  # Fallback sur l'adresse locale si nécessaire
        finally:
            s.close()
        return IP

    # Obtenir le nom d'hôte de la machine
    def get_hostname(self):

        return socket.gethostname()

    # Fonction pour lancer le scan du réseau en utilisant Nmap
    # Nettoyage des résultats précédents et mise à jour de l'interface avec les nouveaux résultats
    def scan_network(self):

        # Vider la liste des résultats du scan précédent
        self.scan_results.clear()

        # Utilisation de nmap pour le scan réseau
        network_range = self.network_entry.get()  # Obtenir la plage réseau à partir de l'entrée utilisateur
        nm = nmap.PortScanner()
        nm.scan(hosts=network_range, arguments='-sT')

        # Nettoyer le tableau existant
        for item in self.tree.get_children():
            self.tree.delete(item)

        # Remplir le tableau avec les résultats du scan nmap
        for ip in nm.all_hosts():
            result = nm[ip]

            # Vérifier si la clé 'tcp' existe dans le dictionnaire result
            if 'tcp' in result:
                open_ports = ', '.join(
                    [str(port) for port in result['tcp'].keys() if result['tcp'][port]['state'] == 'open'])
            else:
                open_ports = "N/A"

            host_name = result['hostnames'][0]['name'] if result['hostnames'] else ""
            self.tree.insert("", tk.END, values=(ip, open_ports, host_name))

            # Ajouter le résultat au stockage des résultats du scan
            self.scan_results.append({
                'ip': ip,
                'open_ports': open_ports,
                'host_name': host_name
            })

        # Liste pour stocker les temps de ping
        ping_times = []

        # Effectuer 20 pings vers l'hôte 8.8.8.8
        target_ip = "8.8.8.8"
        for i in range(20):
            response = ping(target_ip)
            if response is not None:
                ping_times.append(response)

        # Calculer la moyenne des pings
        average_ping_time = 1000 * (sum(ping_times) / len(ping_times)) if len(ping_times) > 0 else 0

        # Mettre à jour l'interface avec la moyenne du ping
        self.average_text.delete(0, tk.END)
        self.average_text.insert(0, f"{average_ping_time:.2f} ms")

        # Afficher le nombre d'hôtes scannés
        num_hosts_scanned = len(nm.all_hosts())
        self.host_count_label.config(text=f"Nombre d'hôtes scannés: {num_hosts_scanned}")

    def export_to_xml(self):
        # Créer un élément racine pour le document XML
        root = ET.Element("network_scan_results")

        # Ajouter la date et l'heure actuelles
        timestamp_element = ET.SubElement(root, "timestamp")
        timestamp_element.text = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # Ajouter le nombre d'hôtes scannés
        num_hosts_scanned_element = ET.SubElement(root, "num_hosts_scanned")
        num_hosts_scanned = len(self.scan_results)
        num_hosts_scanned_element.text = str(num_hosts_scanned)

        # Ajouter la version en dur
        version_element = ET.SubElement(root, "version")
        version_element.text = "1.1"

        # Ajouter le host name du harvester
        nom_harvester_element = ET.SubElement(root, "nom_harvester")
        nom_harvester_element.text = self.get_hostname()

        # Ajouter la moyenne de ping
        average_ping_element = ET.SubElement(root, "average_ping")
        average_ping_element.text = self.average_text.get()

        # Ajouter l'adresse IP de la sonde
        ip_machine_element = ET.SubElement(root, "ip_sonde")
        ip_machine_element.text = self.get_ip()

        # Ajouter chaque résultat du scan comme un élément "host"
        for result in self.scan_results:
            host_element = ET.SubElement(root, "host")
            ip_element = ET.SubElement(host_element, "ip")
            ip_element.text = result['ip']
            open_ports_element = ET.SubElement(host_element, "open_ports")
            open_ports_element.text = result['open_ports']
            host_name_element = ET.SubElement(host_element, "host_name")
            host_name_element.text = result['host_name']

        # Créer l'objet ElementTree
        tree = ET.ElementTree(root)

        # Enregistrer le document XML dans un fichier
        tree.write("scan_results.xml")

    def export_to_serveur(self):
        # Créer un élément racine pour le document XML
        root = ET.Element("network_scan_results")

        # Ajouter la date et l'heure actuelles
        timestamp_element = ET.SubElement(root, "timestamp")
        timestamp_element.text = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # Ajouter le nombre d'hôtes scannés
        num_hosts_scanned_element = ET.SubElement(root, "num_hosts_scanned")
        num_hosts_scanned = len(self.scan_results)
        num_hosts_scanned_element.text = str(num_hosts_scanned)

        # Ajouter la version en dur
        version_element = ET.SubElement(root, "version")
        version_element.text = "1.1"

        nom_harvester_element = ET.SubElement(root, "nom_harvester")
        nom_harvester_element.text = self.get_hostname()

        # Ajouter la moyenne de ping
        average_ping_element = ET.SubElement(root, "average_ping")
        average_ping_element.text = self.average_text.get()

        # Ajouter l'adresse IP de la sonde
        ip_machine_element = ET.SubElement(root, "ip_sonde")
        ip_machine_element.text = self.get_ip()

        # Ajouter chaque résultat du scan comme un élément "host"
        for result in self.scan_results:
            host_element = ET.SubElement(root, "host")
            ip_element = ET.SubElement(host_element, "ip")
            ip_element.text = result['ip']
            open_ports_element = ET.SubElement(host_element, "open_ports")
            open_ports_element.text = result['open_ports']
            host_name_element = ET.SubElement(host_element, "host_name")
            host_name_element.text = result['host_name']

        # Convertir l'élément racine en une chaîne XML
        xml_content = ET.tostring(root).decode()

        # Envoyer les données au serveur
        send_data_to_server(xml_content)


if __name__ == "__main__":
    root = tk.Tk()
    app = NetworkScannerApp(root)
    root.mainloop()
