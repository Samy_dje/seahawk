import requests
import xml.etree.ElementTree as ET


def send_data_to_server(xml_content):
    try:
        # URL du serveur web
        url = "http://192.168.88.151:5000/api/end_point"

        # Définir les en-têtes pour spécifier que vous envoyez des données XML
        headers = {'Content-Type': 'application/xml'}

        # Envoyer la requête POST au serveur
        response = requests.post(url, data=xml_content, headers=headers)

        # Afficher la réponse du serveur
        print(response.text)

    except requests.RequestException as e:
        print(f"Erreur lors de la requête vers le serveur : {e}")


def send_status_to_server(xml_content):
    try:
        # URL du serveur web
        url = "http://192.168.88.151:5000/api/update_status"

        # Définir les en-têtes pour spécifier que vous envoyez des données XML
        headers = {'Content-Type': 'application/xml'}

        # Envoyer la requête POST au serveur
        response = requests.post(url, data=xml_content, headers=headers)

        # Afficher la réponse du serveur
        print(response.text)

    except requests.RequestException as e:
        print(f"Erreur lors de la requête vers le serveur : {e}")
